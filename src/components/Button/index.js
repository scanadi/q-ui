import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {theme} from '../../theme.props'

class Button extends Component {

  static propTypes = {
    label: PropTypes.string,
    disabled: PropTypes.bool,
    type: PropTypes.oneOf(['fill','outline','link']),
    html: PropTypes.oneOf(['button','a','div','span']),
    color: PropTypes.oneOf(Object.keys(theme.colors)),
    padding: PropTypes.oneOf(Object.keys(theme.spacing)),
    margin: PropTypes.oneOf(Object.keys(theme.spacing)),
    rounded: PropTypes.oneOf(Object.keys(theme.borderRadius)),
    size: PropTypes.oneOf(Object.keys(theme.fontSize)),
    onClick: PropTypes.func,
    anchor: PropTypes.object
  }

  static defaultProps = {
    html:'button',
    color: 'blue', 
    padding: '6',
    rounded: 'none',
    margin: '0',
    size: 'base',
    label: 'button',
    anchor: {href:'#', target:'_self'}
 };


   render(){

    const {label, type, color, padding, margin, size, rounded, children, onClick, html, disabled, anchor} = this.props
    const element = React.createElement;
    const isLink = html === 'a'
    const defaultClasses = `py-3 tracking-wider select-none inline-flex justify-center items-center leading-none mx-${margin} rounded-${rounded} ${!disabled ? 'cursor-pointer' : 'opacity-50 cursor-not-allowed'}`
    let typeClasses = '';

    switch(type) { 
      case "fill": { 
        typeClasses = `bg-${color}-600 hover:bg-${color}-700 ${color !== 'white' ? 'text-white' : 'text-black'}`
        break; 
      }
      case "outline": { 
        typeClasses = `bg-transparent hover:bg-${color}-600 hover:${color !== 'white' ? 'text-white' : 'text-black'} text-${color}-600 border border-${color}-600`
        break; 
      } 
      case "link": { 
        typeClasses = `bg-transparent hover:bg-${color}-600 text-${color}-600 hover:${color !== 'white' ? 'text-white' : 'text-black'} underline hover:no-underline`
        break; 
      } 
      default: { 
        typeClasses = `bg-transparent hover:bg-alpha`
        break;              
      } 
    } 

    const style = `px-${padding} text-${size} ${defaultClasses} ${typeClasses}`
    
        
    return(

      element(
        html, 
        {
          className: style,
          onClick:onClick,
          disabled,
          href: isLink ? anchor.href : null,
          target: isLink ? anchor.target: null,
          tabIndex: 0,
          'aria-label': label
        }, 
        children
      )  
    )
  }
}

export default Button