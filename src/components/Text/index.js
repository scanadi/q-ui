import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {theme} from '../../theme.props'

class Text extends Component {

  static propTypes = {
    html: PropTypes.oneOf(['h1','h2','h3','h4','h5','h6','span','p','div','label']),
    display: PropTypes.oneOf(['block','inline','inline-block']),
    leading: PropTypes.oneOf(Object.keys(theme.lineHeight)),
    align: PropTypes.oneOf(['left','right','center', 'justify']),
    color: PropTypes.oneOf(Object.keys(theme.colors)),
    padding: PropTypes.oneOf(Object.keys(theme.spacing)),
    margin: PropTypes.oneOf(Object.keys(theme.spacing)),
    size: PropTypes.oneOf(Object.keys(theme.fontSize)),
    weight: PropTypes.oneOf(Object.keys(theme.fontWeight)),
    tracking: PropTypes.oneOf(Object.keys(theme.letterSpacing)),
    antialiased: PropTypes.bool,
    italic: PropTypes.bool
  }

  static defaultProps = {
    html:'div',
    color: 'black', 
    size: 'base',
    weight: 'normal',
    leading: 'none',
    italic: false,
    antialiased: true
 };


   render(){

    const {display, html, color, padding, margin, size, leading,  children, antialiased, italic, align, weight} = this.props
    const element = React.createElement;

    let style = `${antialiased ? 'antialiased' : ''} ${display ? ' display ' : ''} ${padding ? ' px-'+ padding : ''} ${margin ? ' mb-'+ margin : ''} ${align ? ' text-'+ align : ''} ${italic ? ' italic' : ''} text-${size} text-${color} text-${weight} leading-${leading}`
    style = style.replace(/\s+/g, ' ')          
    
    return(

      element(
        html, 
        {
          className: style,
          'aria-label': children
        }, 
        children
      )  
    )
  }
}

export default Text