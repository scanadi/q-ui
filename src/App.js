import React from 'react';
import Button from './components/Button/'
import Text from './components/Text/'

function App() {
  return (
    <div className="bg-indigo-700 text-white flex h-screen w-screen justify-center items-center ">
      <div className="flex flex-col items-center">
        <Text html={'h1'} display={'block'} color={'white'} weight={'medium'} margin={'12'} align={'center'} size={'8xl'}>Q-UI</Text>
        <Text html={'h2'} display={'block'} color={'blue'} weight={'thin'} margin={'12'} align={'center'} size={'6xl'}>Design System</Text>
        <div className="mt-24 flex flex-col md:flex-row md:items-center md:justify-center w-screen  max-w-6xl flex-wrap">
          <Button padding={'12'} margin={'4'} size={'lg'} color={'white'}>Button</Button>
          <Button padding={'12'} margin={'4'} size={'lg'} type="fill" color={'black'}>Button</Button>
          <Button padding={'12'} margin={'4'} size={'lg'} type="fill" color={'indigo'}>Button</Button>
          <Button padding={'12'} margin={'4'} size={'lg'} type="fill" color={'blue'}>Button</Button>
          <Button padding={'12'} margin={'4'} size={'lg'} type="fill" color={'yellow'}>Button</Button>
          <Button padding={'12'} margin={'4'} size={'lg'} type="outline" color={'white'}>Button</Button>
        </div>
      </div>
    </div>
  );
}

export default App;