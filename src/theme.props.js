import resolveConfig from 'tailwindcss/resolveConfig'
import tailwindConfig from 'tailwindcss/defaultConfig'

const config = resolveConfig(tailwindConfig)

export const theme = config.theme
